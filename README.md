# [Trello Clone](https://trello-react-sandesh.netlify.app/)

This project is a simplified clone of Trello, built using React for the frontend and Trello's RESTful API for backend data management. It allows users to manage boards, lists, cards, checklists, and checklist items similar to how Trello works.

## Features

- **Boards**: Create boards.
- **Lists**: Manage lists within boards (create, view, delete).
- **Cards**: Add cards to lists and delete cards.
- **Checklists**: Create checklists within cards, manage checklist items.
- **Checklist Items**: Add and delete items within checklists.

## Technologies Used

- React
- Axios (for API requests)
- Trello RESTful API

## UI

#### Boards Page
![alt text](./public/screenshots/boardsPage.png) 

#### Lists Page
![alt text](./public/screenshots/listsPage.png) 

#### Card
![alt text](./public/screenshots/card.png)