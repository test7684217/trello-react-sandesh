import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function createCheckList(checkListName, id) {
  const response = await axios.post(
    `https://api.trello.com/1/checklists?idCard=${id}&name=${checkListName}&key=${APIKey}&token=${token}`,
    {
      method: "POST",
    }
  );

  return response.data;
}
