import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function getCheckLists(cardId) {
  const response = await axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${token}`
  );
  return response.data;
}
