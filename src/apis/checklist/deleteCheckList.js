import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function deleteCheckList(id) {
  const response = await axios.delete(
    `https://api.trello.com/1/checklists/${id}?key=${APIKey}&token=${token}`
  );

  return response.data;
}
