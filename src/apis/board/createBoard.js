import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function createBoard(boardName) {
    const response = await axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${token}` );
    return response.data;
}
