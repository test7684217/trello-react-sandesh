import axios from "axios";
import { APIKey, token } from "../constants.js";

export async function getAllBoards() {
  const response = await axios.get(
    `https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${token}`
  );

  return response.data;
}
