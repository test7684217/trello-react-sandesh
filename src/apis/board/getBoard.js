import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function getBoard(boardId) {
    const response = await axios.get(
      `https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${token}`
    );
    return response.data;
  
}
