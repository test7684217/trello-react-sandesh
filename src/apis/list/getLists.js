import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function getLists(boardId) {
    const response = await axios.get(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${token}`
    );

    return response.data;
  
}
