import { token, APIKey } from "../constants.js";

import axios from "axios";

export async function createList(listName, boardId) {
  const response = await axios.post(
    `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${token}`
  );

  return response.data;
}
