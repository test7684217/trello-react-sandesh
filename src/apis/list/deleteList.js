import axios from "axios";
import { APIKey, token } from "../constants.js";

export async function archiveList(listId) {
    const response = await axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${APIKey}&token=${token}`
    );

    return response.data;
  
}
