import axios from "axios";
import { APIKey, token } from "../constants.js";

export async function updateCheckItem(cardId,idCheckItem,state) {
    const response = await axios.put(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?&state=${state}&key=${APIKey}&token=${token}`
    );

    return response.data;
  
}
