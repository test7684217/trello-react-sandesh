import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function deleteCheckListItem(id, idCheckItem) {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${id}/checkItems/${idCheckItem}?key=${APIKey}&token=${token}`
    );
    return response.data;
  
}
