import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function createCheckListItem(checkListItemName, id) {
    const response = await axios.post(
      `https://api.trello.com/1/checklists/${id}/checkItems?name=${checkListItemName}&key=${APIKey}&token=${token}`
    );

    return response.data;
  
}
