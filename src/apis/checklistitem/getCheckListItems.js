import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function getCheckListItems(id) {
    const response = await axios.get(
      `https://api.trello.com/1/checklists/${id}/checkItems?key=${APIKey}&token=${token}`
    );
    return response.data;
  
}
