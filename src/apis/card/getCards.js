import { APIKey, token } from "../constants.js";
import axios from 'axios'

export async function getCards(listId) {
    const response = await axios.get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${token}`
    );

    return response.data;

}
