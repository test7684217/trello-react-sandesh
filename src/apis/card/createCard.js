import { APIKey, token } from "../constants.js";
import axios from "axios";

export async function createCard(cardName, listId) {
  const response = await axios.post(
    `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${token}`
  );

  return response.data;
}
