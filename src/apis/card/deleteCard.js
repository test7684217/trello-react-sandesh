import { APIKey, token } from "../constants.js";
import axios from "axios";
export async function archiveCard(id) {
  const response = await axios.delete(
    `https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${token}`
  );

  return response.data;
}
